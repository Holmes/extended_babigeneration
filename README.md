Prerequisites
=============

* SBCL or Lispworks
* Quicklisp
* GF

-------------------------------------------------------------------------------

Eng-DCEC
========

```DCEC.gf``` is the core abstract syntax. ```DCECExt.gf``` is an
extension of the core abstract syntax with some verbs and
agents. ```Eng.gf``` is the core concrete syntax, and ```EngExt.gf```
is the concrete syntax extended.

-------------------------------------------------------------------------------

Setting up the web interface
========



* Download or clone the repository

* Spin up the GF server:

```bash
cd path-to/GF/files
gf -make QA150.gf QA150Eng.gf
gf -server --document-root=.
```

* Spin up the Lisp server (for the interactive parser's web interface
   and for transform from simple abstract GF into concrete DCEC*).

```lisp
(load "./loader.lisp")
(load "./snark-20120808r02/snark-system.lisp")
(make-snark-system)
(load "./snark-20120808r02/snark-interface.lisp")
(in-package :cl-user)
(load "./lisp/generate.lisp")
(eng-dcec::generate-data-set nil  1000 8 4 "/Users/naveensundarg/projects/learning_to_reason_evaluation/extended_babiGeneration/data/dataset_1_1000.lisp")
```

The function for generating datasets is:


```lisp
generate-data-set (grammar size story_size num_questions filename)
```

Right now, the grammar is hardcoded is configs.lisp. Ideally,
```generate-data-set``` would take the grammar name as an input. The
first parameter is reserved for that.

(For your interface to be accessed remotely, edit
```*gf-server-url*``` in ```configs.lisp``` to point to where the GF server is running. )
