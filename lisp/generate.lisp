(in-package :eng-dcec)


(defun is-query (abstract-string)
  (cl-ppcre:scan "AfraidOfIndQuery" abstract-string))

(defun is-declaration (abstract-string)
  (not (cl-ppcre:scan "Query" abstract-string)))

(defun generate-declaration (grammar)
  (let ((generated (random-gf)))
    (if  (is-declaration (first generated)) generated (generate-declaration grammar))))


(defun generate-query (grammar)
  (let ((generated (random-gf)))
    (if (is-query (first generated)) generated (generate-query grammar))))

(defun generate-n-sentences (grammar n generator &key (constraint
					       (lambda (new prior) (not (member new prior :test #'equalp)) )))
  (labels ((generate-unique (current)
	     (let ((generated (funcall generator grammar)))
	       (if (funcall constraint generated current) ;; if the generated satisfies the constraint
		   generated
		   (generate-unique current)))))
    (if (zerop n)
	()
	(let ((current (generate-n-sentences grammar (- n 1) generator :constraint constraint)))
	  (cons (generate-unique current) current)))))


(defun process-expression (abstract-string)
  (flet ((make-expression (string)
	   (read-from-string (concatenate 'string "(" string")")))
	 (query-arg (query) (second query)))
    (let ((expr (make-expression abstract-string)))
      (cond ((is-query abstract-string)
	     `(cl-user::AFRAIDOF ,(query-arg expr) cl-user::?x))
	    ((equalp "COMPLETEPHRASE" (symbol-name  (first expr)))
	     (second  expr))))))

(defparameter *axiom-1*
  '(snark::forall (?class1 ?ind ?z) 
    (snark::implies (snark::and (cl-user::MEMBERSHIP ?class1 ?ind) (cl-user::AFRAIDOF ?class1 ?class2)) (cl-user::AFRAIDOF ?ind ?class2))))


(defparameter *axiom-2*
  '(snark::forall (?class1 ?ind ?z) 
    (snark::implies (snark::and (cl-user::MEMBERSHIP ?class1 ?ind) (cl-user::MEMBERSHIP ?class2 ?ind))
     (snark::= ?class1 ?class2))))

(defparameter *not-equal-1*
  '(snark-user::and 
    (snark-user::not (snark-user::= WOLVES CATS))
    (snark-user::not (snark-user::= WOLVES SHEEP))
    (snark-user::not (snark-user::= WOLVES MICE))
    (snark-user::not (snark-user::= CATS MICE))
    (snark-user::not (snark-user::= CATS SHEEP))
    (snark-user::not (snark-user::= SHEEP MICE))))

(defparameter *not-equal-2*
  '(snark-user::and 
    (snark-user::not (snark-user::= EMILY JESSICA))
    (snark-user::not (snark-user::= EMILY GERTRUDE))
    (snark-user::not (snark-user::= EMILY WINONA))
    (snark-user::not (snark-user::= JESSICA GERTRUDE))
    (snark-user::not (snark-user::= JESSICA WINONA))
    (snark-user::not (snark-user::= GERTRUDE WINONA))))

(defparameter *axioms*
  (list *axiom-1* *axiom-2* *not-equal-1* *not-equal-2*))

(defun answer (story-forms query-form)
  (let ((answer (snark-user::prove-from-axioms-and-get-answers
		 (cons eng-dcec::*axiom-1* story-forms)
		 query-form '(cl-user::?X))))
    (if (equalp  answer "")
	"Incomplete"
	(first (read-from-string (first  answer))))))


(defun generate-story (grammar story_size num_questions)
  (labels ((postprocessor (generated puncutation)
	      (mapcar #'(lambda (x) (concatenate 'string (first (second x)) puncutation)) generated)))
      (let* ((story_structs (generate-n-sentences grammar story_size #'generate-declaration))
	     (question_structs (generate-n-sentences grammar num_questions #'generate-query))
	     (story_sentences (postprocessor story_structs "."))
	     (questions (postprocessor question_structs "?"))
	     (story_forms (mapcar (lambda (x) (process-expression (first x))) story_structs))
	     (question_forms (mapcar (lambda (x) (process-expression (first x))) question_structs)))
	(list :story story_sentences
	      :questions questions
	      :story_forms story_forms
	      :question_forms question_forms
	      :answers (mapcar (lambda (question question-form) 
				 (list question (answer story_forms question-form))) 
			       questions
			       question_forms)
	      :story-is-consistent (snark-user::consistent? (append *axioms* story_forms) 5)))))



(defun generate-data-set (grammar size story_size num_questions filename)
  (with-open-file (str filename
                     :direction  :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
    (dotimes  (num size nil)
      (format str "~%~%;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;~%")
      (format str (concatenate 'string ";;;;;;;;;; Story   " (princ-to-string (1+ num)) " ;;;;;;;;;;;;;~%"))
      (format str ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;~%")
      (pprint (append (list :number num) (generate-story grammar story_size num_questions)) str))))





(defun generate-data-set-babi-format (grammar size story_size num_questions filename)
  (with-open-file (str filename
                     :direction  :output
                     :if-exists :supersede
                     :if-does-not-exist :create)
    (dotimes  (num size nil)
      (let* ((data-item (generate-story grammar story_size num_questions))
	     (story (second data-item))
	     (questions-and-answers (tenth data-item))
	     (line-number 1))
	(mapcar (lambda (line) (format str "~a ~a~%" line-number line) 
			(incf line-number))
		story)
	(mapcar (lambda (line) (format str  "~a ~a ~a ~%" line-number (first line) (string-downcase (second line)))
			(incf line-number)) 
		questions-and-answers)))))



