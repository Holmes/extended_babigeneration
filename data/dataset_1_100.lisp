

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   1 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 0 :STORY
 ("wolves are afraid of mice." "jessica is a cat." "cats are afraid of cats."
  "wolves are afraid of cats." "mice are afraid of wolves."
  "wolves are afraid of wolves." "winona is a mouse."
  "sheep are afraid of mice.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is gertrude afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES MICE) (MEMBERSHIP CATS JESSICA) (AFRAIDOF CATS CATS)
  (AFRAIDOF WOLVES CATS) (AFRAIDOF MICE WOLVES) (AFRAIDOF WOLVES WOLVES)
  (MEMBERSHIP MICE WINONA) (AFRAIDOF SHEEP MICE))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is jessica afraid of?" (CATS))
  ("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete")
  ("what is winona afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   2 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 1 :STORY
 ("sheep are afraid of mice." "emily is a cat." "cats are afraid of sheep."
  "emily is a sheep." "sheep are afraid of wolves." "emily is a wolf."
  "wolves are afraid of cats." "mice are afraid of sheep.")
 :QUESTIONS
 ("what is winona afraid of?" "what is gertrude afraid of?"
  "what is emily afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP MICE) (MEMBERSHIP CATS EMILY) (AFRAIDOF CATS SHEEP)
  (MEMBERSHIP SHEEP EMILY) (AFRAIDOF SHEEP WOLVES) (MEMBERSHIP WOLVES EMILY)
  (AFRAIDOF WOLVES CATS) (AFRAIDOF MICE SHEEP))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is winona afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" (SHEEP))
  ("what is jessica afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   3 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 2 :STORY
 ("wolves are afraid of cats." "sheep are afraid of wolves."
  "winona is a wolf." "emily is a mouse." "sheep are afraid of mice."
  "cats are afraid of wolves." "jessica is a mouse."
  "cats are afraid of mice.")
 :QUESTIONS
 ("what is emily afraid of?" "what is jessica afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES CATS) (AFRAIDOF SHEEP WOLVES) (MEMBERSHIP WOLVES WINONA)
  (MEMBERSHIP MICE EMILY) (AFRAIDOF SHEEP MICE) (AFRAIDOF CATS WOLVES)
  (MEMBERSHIP MICE JESSICA) (AFRAIDOF CATS MICE))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is emily afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" (CATS))
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   4 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 3 :STORY
 ("mice are afraid of cats." "winona is a wolf." "wolves are afraid of mice."
  "mice are afraid of wolves." "winona is a sheep."
  "sheep are afraid of sheep." "cats are afraid of wolves."
  "cats are afraid of mice.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE CATS) (MEMBERSHIP WOLVES WINONA) (AFRAIDOF WOLVES MICE)
  (AFRAIDOF MICE WOLVES) (MEMBERSHIP SHEEP WINONA) (AFRAIDOF SHEEP SHEEP)
  (AFRAIDOF CATS WOLVES) (AFRAIDOF CATS MICE))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" (MICE))
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   5 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 4 :STORY
 ("jessica is a sheep." "jessica is a wolf." "cats are afraid of sheep."
  "emily is a sheep." "winona is a mouse." "sheep are afraid of cats."
  "emily is a wolf." "wolves are afraid of sheep.")
 :QUESTIONS
 ("what is winona afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP SHEEP JESSICA) (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF CATS SHEEP)
  (MEMBERSHIP SHEEP EMILY) (MEMBERSHIP MICE WINONA) (AFRAIDOF SHEEP CATS)
  (MEMBERSHIP WOLVES EMILY) (AFRAIDOF WOLVES SHEEP))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is winona afraid of?" "Incomplete")
  ("what is jessica afraid of?" (CATS)) ("what is emily afraid of?" (CATS))
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   6 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 5 :STORY
 ("mice are afraid of cats." "sheep are afraid of cats." "gertrude is a sheep."
  "mice are afraid of sheep." "mice are afraid of wolves."
  "wolves are afraid of sheep." "gertrude is a wolf." "winona is a wolf.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is gertrude afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE CATS) (AFRAIDOF SHEEP CATS) (MEMBERSHIP SHEEP GERTRUDE)
  (AFRAIDOF MICE SHEEP) (AFRAIDOF MICE WOLVES) (AFRAIDOF WOLVES SHEEP)
  (MEMBERSHIP WOLVES GERTRUDE) (MEMBERSHIP WOLVES WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" (SHEEP)) ("what is gertrude afraid of?" (CATS))
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   7 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 6 :STORY
 ("wolves are afraid of mice." "mice are afraid of sheep."
  "mice are afraid of mice." "jessica is a sheep." "gertrude is a mouse."
  "gertrude is a sheep." "cats are afraid of cats." "emily is a sheep.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is gertrude afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES MICE) (AFRAIDOF MICE SHEEP) (AFRAIDOF MICE MICE)
  (MEMBERSHIP SHEEP JESSICA) (MEMBERSHIP MICE GERTRUDE)
  (MEMBERSHIP SHEEP GERTRUDE) (AFRAIDOF CATS CATS) (MEMBERSHIP SHEEP EMILY))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (SHEEP))
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   8 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 7 :STORY
 ("mice are afraid of mice." "mice are afraid of cats." "gertrude is a cat."
  "cats are afraid of wolves." "sheep are afraid of cats." "emily is a cat."
  "cats are afraid of mice." "mice are afraid of wolves.")
 :QUESTIONS
 ("what is emily afraid of?" "what is gertrude afraid of?"
  "what is winona afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE MICE) (AFRAIDOF MICE CATS) (MEMBERSHIP CATS GERTRUDE)
  (AFRAIDOF CATS WOLVES) (AFRAIDOF SHEEP CATS) (MEMBERSHIP CATS EMILY)
  (AFRAIDOF CATS MICE) (AFRAIDOF MICE WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is emily afraid of?" (WOLVES))
  ("what is gertrude afraid of?" (WOLVES))
  ("what is winona afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   9 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 8 :STORY
 ("wolves are afraid of mice." "jessica is a mouse." "winona is a sheep."
  "gertrude is a cat." "emily is a sheep." "mice are afraid of sheep."
  "emily is a mouse." "cats are afraid of cats.")
 :QUESTIONS
 ("what is winona afraid of?" "what is emily afraid of?"
  "what is jessica afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES MICE) (MEMBERSHIP MICE JESSICA) (MEMBERSHIP SHEEP WINONA)
  (MEMBERSHIP CATS GERTRUDE) (MEMBERSHIP SHEEP EMILY) (AFRAIDOF MICE SHEEP)
  (MEMBERSHIP MICE EMILY) (AFRAIDOF CATS CATS))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is winona afraid of?" "Incomplete")
  ("what is emily afraid of?" (SHEEP)) ("what is jessica afraid of?" (SHEEP))
  ("what is gertrude afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   10 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 9 :STORY
 ("emily is a mouse." "gertrude is a wolf." "winona is a cat."
  "cats are afraid of sheep." "emily is a wolf." "gertrude is a mouse."
  "sheep are afraid of wolves." "winona is a wolf.")
 :QUESTIONS
 ("what is emily afraid of?" "what is gertrude afraid of?"
  "what is jessica afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE EMILY) (MEMBERSHIP WOLVES GERTRUDE) (MEMBERSHIP CATS WINONA)
  (AFRAIDOF CATS SHEEP) (MEMBERSHIP WOLVES EMILY) (MEMBERSHIP MICE GERTRUDE)
  (AFRAIDOF SHEEP WOLVES) (MEMBERSHIP WOLVES WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" (SHEEP)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   11 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 10 :STORY
 ("wolves are afraid of mice." "winona is a wolf." "gertrude is a cat."
  "gertrude is a sheep." "winona is a sheep." "sheep are afraid of mice."
  "cats are afraid of cats." "winona is a cat.")
 :QUESTIONS
 ("what is winona afraid of?" "what is gertrude afraid of?"
  "what is emily afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES MICE) (MEMBERSHIP WOLVES WINONA) (MEMBERSHIP CATS GERTRUDE)
  (MEMBERSHIP SHEEP GERTRUDE) (MEMBERSHIP SHEEP WINONA) (AFRAIDOF SHEEP MICE)
  (AFRAIDOF CATS CATS) (MEMBERSHIP CATS WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is winona afraid of?" (MICE)) ("what is gertrude afraid of?" (CATS))
  ("what is emily afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   12 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 11 :STORY
 ("jessica is a mouse." "jessica is a cat." "gertrude is a wolf."
  "cats are afraid of cats." "mice are afraid of wolves."
  "sheep are afraid of mice." "jessica is a wolf." "winona is a mouse.")
 :QUESTIONS
 ("what is winona afraid of?" "what is jessica afraid of?"
  "what is gertrude afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE JESSICA) (MEMBERSHIP CATS JESSICA)
  (MEMBERSHIP WOLVES GERTRUDE) (AFRAIDOF CATS CATS) (AFRAIDOF MICE WOLVES)
  (AFRAIDOF SHEEP MICE) (MEMBERSHIP WOLVES JESSICA) (MEMBERSHIP MICE WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is winona afraid of?" (WOLVES))
  ("what is jessica afraid of?" (WOLVES))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   13 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 12 :STORY
 ("jessica is a sheep." "winona is a mouse." "winona is a cat."
  "emily is a mouse." "mice are afraid of wolves." "gertrude is a mouse."
  "cats are afraid of sheep." "sheep are afraid of wolves.")
 :QUESTIONS
 ("what is emily afraid of?" "what is jessica afraid of?"
  "what is gertrude afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP SHEEP JESSICA) (MEMBERSHIP MICE WINONA) (MEMBERSHIP CATS WINONA)
  (MEMBERSHIP MICE EMILY) (AFRAIDOF MICE WOLVES) (MEMBERSHIP MICE GERTRUDE)
  (AFRAIDOF CATS SHEEP) (AFRAIDOF SHEEP WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is emily afraid of?" (WOLVES)) ("what is jessica afraid of?" (WOLVES))
  ("what is gertrude afraid of?" (WOLVES))
  ("what is winona afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   14 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 13 :STORY
 ("emily is a cat." "mice are afraid of cats." "wolves are afraid of sheep."
  "sheep are afraid of mice." "wolves are afraid of mice."
  "wolves are afraid of wolves." "mice are afraid of wolves."
  "emily is a wolf.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is emily afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS EMILY) (AFRAIDOF MICE CATS) (AFRAIDOF WOLVES SHEEP)
  (AFRAIDOF SHEEP MICE) (AFRAIDOF WOLVES MICE) (AFRAIDOF WOLVES WOLVES)
  (AFRAIDOF MICE WOLVES) (MEMBERSHIP WOLVES EMILY))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete")
  ("what is emily afraid of?" (SHEEP))
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   15 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 14 :STORY
 ("jessica is a wolf." "sheep are afraid of sheep." "cats are afraid of mice."
  "winona is a sheep." "sheep are afraid of cats." "wolves are afraid of cats."
  "gertrude is a wolf." "jessica is a mouse.")
 :QUESTIONS
 ("what is winona afraid of?" "what is emily afraid of?"
  "what is gertrude afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES JESSICA) (AFRAIDOF SHEEP SHEEP) (AFRAIDOF CATS MICE)
  (MEMBERSHIP SHEEP WINONA) (AFRAIDOF SHEEP CATS) (AFRAIDOF WOLVES CATS)
  (MEMBERSHIP WOLVES GERTRUDE) (MEMBERSHIP MICE JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is winona afraid of?" (SHEEP))
  ("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (CATS)) ("what is jessica afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   16 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 15 :STORY
 ("gertrude is a cat." "mice are afraid of cats." "sheep are afraid of cats."
  "jessica is a cat." "emily is a wolf." "sheep are afraid of mice."
  "cats are afraid of wolves." "jessica is a wolf.")
 :QUESTIONS
 ("what is emily afraid of?" "what is winona afraid of?"
  "what is jessica afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS GERTRUDE) (AFRAIDOF MICE CATS) (AFRAIDOF SHEEP CATS)
  (MEMBERSHIP CATS JESSICA) (MEMBERSHIP WOLVES EMILY) (AFRAIDOF SHEEP MICE)
  (AFRAIDOF CATS WOLVES) (MEMBERSHIP WOLVES JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete")
  ("what is jessica afraid of?" (WOLVES))
  ("what is gertrude afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   17 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 16 :STORY
 ("emily is a wolf." "cats are afraid of mice." "emily is a mouse."
  "emily is a sheep." "sheep are afraid of mice." "mice are afraid of sheep."
  "wolves are afraid of sheep." "gertrude is a cat.")
 :QUESTIONS
 ("what is winona afraid of?" "what is gertrude afraid of?"
  "what is jessica afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES EMILY) (AFRAIDOF CATS MICE) (MEMBERSHIP MICE EMILY)
  (MEMBERSHIP SHEEP EMILY) (AFRAIDOF SHEEP MICE) (AFRAIDOF MICE SHEEP)
  (AFRAIDOF WOLVES SHEEP) (MEMBERSHIP CATS GERTRUDE))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is winona afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (MICE))
  ("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" (SHEEP)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   18 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 17 :STORY
 ("mice are afraid of sheep." "jessica is a sheep." "jessica is a cat."
  "jessica is a wolf." "cats are afraid of wolves."
  "wolves are afraid of mice." "sheep are afraid of mice."
  "sheep are afraid of sheep.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is gertrude afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE SHEEP) (MEMBERSHIP SHEEP JESSICA) (MEMBERSHIP CATS JESSICA)
  (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF CATS WOLVES) (AFRAIDOF WOLVES MICE)
  (AFRAIDOF SHEEP MICE) (AFRAIDOF SHEEP SHEEP))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is jessica afraid of?" (MICE))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   19 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 18 :STORY
 ("jessica is a cat." "winona is a mouse." "jessica is a mouse."
  "mice are afraid of cats." "jessica is a wolf." "sheep are afraid of mice."
  "mice are afraid of sheep." "jessica is a sheep.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS JESSICA) (MEMBERSHIP MICE WINONA) (MEMBERSHIP MICE JESSICA)
  (AFRAIDOF MICE CATS) (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF SHEEP MICE)
  (AFRAIDOF MICE SHEEP) (MEMBERSHIP SHEEP JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" (CATS))
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   20 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 19 :STORY
 ("mice are afraid of sheep." "emily is a wolf." "emily is a sheep."
  "cats are afraid of mice." "sheep are afraid of sheep."
  "mice are afraid of wolves." "winona is a sheep." "cats are afraid of cats.")
 :QUESTIONS
 ("what is emily afraid of?" "what is jessica afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE SHEEP) (MEMBERSHIP WOLVES EMILY) (MEMBERSHIP SHEEP EMILY)
  (AFRAIDOF CATS MICE) (AFRAIDOF SHEEP SHEEP) (AFRAIDOF MICE WOLVES)
  (MEMBERSHIP SHEEP WINONA) (AFRAIDOF CATS CATS))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is emily afraid of?" (SHEEP))
  ("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" (SHEEP))
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   21 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 20 :STORY
 ("sheep are afraid of cats." "gertrude is a cat." "jessica is a sheep."
  "winona is a sheep." "emily is a mouse." "cats are afraid of wolves."
  "mice are afraid of sheep." "gertrude is a mouse.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP CATS) (MEMBERSHIP CATS GERTRUDE) (MEMBERSHIP SHEEP JESSICA)
  (MEMBERSHIP SHEEP WINONA) (MEMBERSHIP MICE EMILY) (AFRAIDOF CATS WOLVES)
  (AFRAIDOF MICE SHEEP) (MEMBERSHIP MICE GERTRUDE))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" (CATS)) ("what is emily afraid of?" (SHEEP))
  ("what is winona afraid of?" (CATS))
  ("what is gertrude afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   22 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 21 :STORY
 ("gertrude is a cat." "winona is a sheep." "wolves are afraid of sheep."
  "jessica is a cat." "cats are afraid of sheep." "emily is a cat."
  "mice are afraid of cats." "mice are afraid of wolves.")
 :QUESTIONS
 ("what is emily afraid of?" "what is winona afraid of?"
  "what is gertrude afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS GERTRUDE) (MEMBERSHIP SHEEP WINONA) (AFRAIDOF WOLVES SHEEP)
  (MEMBERSHIP CATS JESSICA) (AFRAIDOF CATS SHEEP) (MEMBERSHIP CATS EMILY)
  (AFRAIDOF MICE CATS) (AFRAIDOF MICE WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is emily afraid of?" (SHEEP))
  ("what is winona afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (SHEEP))
  ("what is jessica afraid of?" (SHEEP)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   23 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 22 :STORY
 ("sheep are afraid of sheep." "sheep are afraid of mice." "winona is a wolf."
  "cats are afraid of wolves." "jessica is a wolf."
  "wolves are afraid of wolves." "emily is a wolf."
  "mice are afraid of wolves.")
 :QUESTIONS
 ("what is winona afraid of?" "what is gertrude afraid of?"
  "what is jessica afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP SHEEP) (AFRAIDOF SHEEP MICE) (MEMBERSHIP WOLVES WINONA)
  (AFRAIDOF CATS WOLVES) (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF WOLVES WOLVES)
  (MEMBERSHIP WOLVES EMILY) (AFRAIDOF MICE WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is winona afraid of?" (WOLVES))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" (WOLVES))
  ("what is emily afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   24 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 23 :STORY
 ("emily is a cat." "gertrude is a mouse." "winona is a cat."
  "emily is a mouse." "mice are afraid of sheep." "wolves are afraid of mice."
  "wolves are afraid of cats." "mice are afraid of mice.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is winona afraid of?"
  "what is emily afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS EMILY) (MEMBERSHIP MICE GERTRUDE) (MEMBERSHIP CATS WINONA)
  (MEMBERSHIP MICE EMILY) (AFRAIDOF MICE SHEEP) (AFRAIDOF WOLVES MICE)
  (AFRAIDOF WOLVES CATS) (AFRAIDOF MICE MICE))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (SHEEP))
  ("what is winona afraid of?" "Incomplete")
  ("what is emily afraid of?" (SHEEP))
  ("what is jessica afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   25 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 24 :STORY
 ("cats are afraid of wolves." "gertrude is a sheep."
  "mice are afraid of cats." "emily is a cat." "emily is a sheep."
  "mice are afraid of sheep." "jessica is a mouse." "winona is a wolf.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is gertrude afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS WOLVES) (MEMBERSHIP SHEEP GERTRUDE) (AFRAIDOF MICE CATS)
  (MEMBERSHIP CATS EMILY) (MEMBERSHIP SHEEP EMILY) (AFRAIDOF MICE SHEEP)
  (MEMBERSHIP MICE JESSICA) (MEMBERSHIP WOLVES WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is jessica afraid of?" (CATS)) ("what is emily afraid of?" (WOLVES))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   26 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 25 :STORY
 ("mice are afraid of mice." "winona is a mouse." "sheep are afraid of mice."
  "gertrude is a mouse." "emily is a sheep." "wolves are afraid of mice."
  "emily is a cat." "winona is a wolf.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE MICE) (MEMBERSHIP MICE WINONA) (AFRAIDOF SHEEP MICE)
  (MEMBERSHIP MICE GERTRUDE) (MEMBERSHIP SHEEP EMILY) (AFRAIDOF WOLVES MICE)
  (MEMBERSHIP CATS EMILY) (MEMBERSHIP WOLVES WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" (MICE)) ("what is winona afraid of?" (MICE))
  ("what is gertrude afraid of?" (MICE)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   27 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 26 :STORY
 ("jessica is a wolf." "cats are afraid of cats." "winona is a wolf."
  "emily is a mouse." "gertrude is a cat." "wolves are afraid of wolves."
  "sheep are afraid of mice." "sheep are afraid of sheep.")
 :QUESTIONS
 ("what is winona afraid of?" "what is emily afraid of?"
  "what is gertrude afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES JESSICA) (AFRAIDOF CATS CATS) (MEMBERSHIP WOLVES WINONA)
  (MEMBERSHIP MICE EMILY) (MEMBERSHIP CATS GERTRUDE) (AFRAIDOF WOLVES WOLVES)
  (AFRAIDOF SHEEP MICE) (AFRAIDOF SHEEP SHEEP))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is winona afraid of?" (WOLVES))
  ("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (CATS))
  ("what is jessica afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   28 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 27 :STORY
 ("cats are afraid of cats." "cats are afraid of wolves."
  "cats are afraid of sheep." "winona is a sheep." "gertrude is a sheep."
  "sheep are afraid of cats." "jessica is a wolf." "emily is a sheep.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS CATS) (AFRAIDOF CATS WOLVES) (AFRAIDOF CATS SHEEP)
  (MEMBERSHIP SHEEP WINONA) (MEMBERSHIP SHEEP GERTRUDE) (AFRAIDOF SHEEP CATS)
  (MEMBERSHIP WOLVES JESSICA) (MEMBERSHIP SHEEP EMILY))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" (CATS)) ("what is winona afraid of?" (CATS))
  ("what is gertrude afraid of?" (CATS)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   29 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 28 :STORY
 ("jessica is a wolf." "jessica is a cat." "winona is a wolf."
  "emily is a cat." "sheep are afraid of mice." "mice are afraid of mice."
  "wolves are afraid of wolves." "mice are afraid of wolves.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES JESSICA) (MEMBERSHIP CATS JESSICA)
  (MEMBERSHIP WOLVES WINONA) (MEMBERSHIP CATS EMILY) (AFRAIDOF SHEEP MICE)
  (AFRAIDOF MICE MICE) (AFRAIDOF WOLVES WOLVES) (AFRAIDOF MICE WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" (WOLVES))
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   30 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 29 :STORY
 ("cats are afraid of wolves." "jessica is a wolf." "cats are afraid of sheep."
  "jessica is a mouse." "sheep are afraid of sheep." "gertrude is a sheep."
  "winona is a sheep." "sheep are afraid of wolves.")
 :QUESTIONS
 ("what is winona afraid of?" "what is emily afraid of?"
  "what is jessica afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS WOLVES) (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF CATS SHEEP)
  (MEMBERSHIP MICE JESSICA) (AFRAIDOF SHEEP SHEEP) (MEMBERSHIP SHEEP GERTRUDE)
  (MEMBERSHIP SHEEP WINONA) (AFRAIDOF SHEEP WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is winona afraid of?" (SHEEP))
  ("what is emily afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (SHEEP)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   31 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 30 :STORY
 ("cats are afraid of wolves." "emily is a mouse." "wolves are afraid of mice."
  "emily is a cat." "winona is a cat." "sheep are afraid of mice."
  "mice are afraid of mice." "sheep are afraid of wolves.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is emily afraid of?"
  "what is jessica afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS WOLVES) (MEMBERSHIP MICE EMILY) (AFRAIDOF WOLVES MICE)
  (MEMBERSHIP CATS EMILY) (MEMBERSHIP CATS WINONA) (AFRAIDOF SHEEP MICE)
  (AFRAIDOF MICE MICE) (AFRAIDOF SHEEP WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" (MICE))
  ("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   32 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 31 :STORY
 ("gertrude is a sheep." "emily is a mouse." "emily is a wolf."
  "winona is a sheep." "cats are afraid of mice." "jessica is a mouse."
  "wolves are afraid of mice." "jessica is a sheep.")
 :QUESTIONS
 ("what is emily afraid of?" "what is gertrude afraid of?"
  "what is jessica afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP SHEEP GERTRUDE) (MEMBERSHIP MICE EMILY) (MEMBERSHIP WOLVES EMILY)
  (MEMBERSHIP SHEEP WINONA) (AFRAIDOF CATS MICE) (MEMBERSHIP MICE JESSICA)
  (AFRAIDOF WOLVES MICE) (MEMBERSHIP SHEEP JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is emily afraid of?" (MICE))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   33 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 32 :STORY
 ("jessica is a sheep." "mice are afraid of wolves." "gertrude is a sheep."
  "wolves are afraid of wolves." "cats are afraid of wolves."
  "sheep are afraid of sheep." "cats are afraid of sheep."
  "sheep are afraid of mice.")
 :QUESTIONS
 ("what is emily afraid of?" "what is winona afraid of?"
  "what is jessica afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP SHEEP JESSICA) (AFRAIDOF MICE WOLVES) (MEMBERSHIP SHEEP GERTRUDE)
  (AFRAIDOF WOLVES WOLVES) (AFRAIDOF CATS WOLVES) (AFRAIDOF SHEEP SHEEP)
  (AFRAIDOF CATS SHEEP) (AFRAIDOF SHEEP MICE))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete")
  ("what is jessica afraid of?" (SHEEP))
  ("what is gertrude afraid of?" (SHEEP)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   34 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 33 :STORY
 ("emily is a cat." "jessica is a sheep." "winona is a wolf."
  "sheep are afraid of wolves." "gertrude is a sheep."
  "mice are afraid of sheep." "sheep are afraid of sheep."
  "mice are afraid of mice.")
 :QUESTIONS
 ("what is emily afraid of?" "what is winona afraid of?"
  "what is jessica afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS EMILY) (MEMBERSHIP SHEEP JESSICA) (MEMBERSHIP WOLVES WINONA)
  (AFRAIDOF SHEEP WOLVES) (MEMBERSHIP SHEEP GERTRUDE) (AFRAIDOF MICE SHEEP)
  (AFRAIDOF SHEEP SHEEP) (AFRAIDOF MICE MICE))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete")
  ("what is jessica afraid of?" (WOLVES))
  ("what is gertrude afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   35 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 34 :STORY
 ("wolves are afraid of sheep." "emily is a mouse." "gertrude is a sheep."
  "sheep are afraid of cats." "winona is a mouse." "gertrude is a mouse."
  "sheep are afraid of mice." "emily is a cat.")
 :QUESTIONS
 ("what is winona afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES SHEEP) (MEMBERSHIP MICE EMILY) (MEMBERSHIP SHEEP GERTRUDE)
  (AFRAIDOF SHEEP CATS) (MEMBERSHIP MICE WINONA) (MEMBERSHIP MICE GERTRUDE)
  (AFRAIDOF SHEEP MICE) (MEMBERSHIP CATS EMILY))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is winona afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   36 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 35 :STORY
 ("winona is a wolf." "cats are afraid of cats." "mice are afraid of cats."
  "cats are afraid of mice." "emily is a sheep." "jessica is a mouse."
  "sheep are afraid of cats." "winona is a sheep.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is gertrude afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES WINONA) (AFRAIDOF CATS CATS) (AFRAIDOF MICE CATS)
  (AFRAIDOF CATS MICE) (MEMBERSHIP SHEEP EMILY) (MEMBERSHIP MICE JESSICA)
  (AFRAIDOF SHEEP CATS) (MEMBERSHIP SHEEP WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is jessica afraid of?" (CATS)) ("what is winona afraid of?" (CATS))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   37 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 36 :STORY
 ("emily is a wolf." "winona is a wolf." "cats are afraid of cats."
  "wolves are afraid of cats." "mice are afraid of cats." "winona is a mouse."
  "emily is a mouse." "cats are afraid of wolves.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES EMILY) (MEMBERSHIP WOLVES WINONA) (AFRAIDOF CATS CATS)
  (AFRAIDOF WOLVES CATS) (AFRAIDOF MICE CATS) (MEMBERSHIP MICE WINONA)
  (MEMBERSHIP MICE EMILY) (AFRAIDOF CATS WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" (CATS)) ("what is winona afraid of?" (CATS))
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   38 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 37 :STORY
 ("jessica is a cat." "gertrude is a mouse." "sheep are afraid of wolves."
  "winona is a mouse." "cats are afraid of mice." "sheep are afraid of cats."
  "cats are afraid of wolves." "sheep are afraid of mice.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is emily afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS JESSICA) (MEMBERSHIP MICE GERTRUDE) (AFRAIDOF SHEEP WOLVES)
  (MEMBERSHIP MICE WINONA) (AFRAIDOF CATS MICE) (AFRAIDOF SHEEP CATS)
  (AFRAIDOF CATS WOLVES) (AFRAIDOF SHEEP MICE))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" (MICE))
  ("what is winona afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   39 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 38 :STORY
 ("mice are afraid of mice." "mice are afraid of cats."
  "sheep are afraid of cats." "jessica is a cat." "jessica is a wolf."
  "sheep are afraid of wolves." "cats are afraid of cats."
  "wolves are afraid of wolves.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is emily afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE MICE) (AFRAIDOF MICE CATS) (AFRAIDOF SHEEP CATS)
  (MEMBERSHIP CATS JESSICA) (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF SHEEP WOLVES)
  (AFRAIDOF CATS CATS) (AFRAIDOF WOLVES WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" (CATS))
  ("what is winona afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   40 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 39 :STORY
 ("wolves are afraid of mice." "cats are afraid of wolves."
  "sheep are afraid of cats." "gertrude is a cat." "mice are afraid of mice."
  "winona is a sheep." "jessica is a mouse." "sheep are afraid of sheep.")
 :QUESTIONS
 ("what is winona afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES MICE) (AFRAIDOF CATS WOLVES) (AFRAIDOF SHEEP CATS)
  (MEMBERSHIP CATS GERTRUDE) (AFRAIDOF MICE MICE) (MEMBERSHIP SHEEP WINONA)
  (MEMBERSHIP MICE JESSICA) (AFRAIDOF SHEEP SHEEP))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is winona afraid of?" (CATS)) ("what is jessica afraid of?" (MICE))
  ("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   41 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 40 :STORY
 ("gertrude is a mouse." "mice are afraid of cats."
  "wolves are afraid of wolves." "winona is a mouse."
  "wolves are afraid of sheep." "emily is a cat." "sheep are afraid of sheep."
  "winona is a cat.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is emily afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE GERTRUDE) (AFRAIDOF MICE CATS) (AFRAIDOF WOLVES WOLVES)
  (MEMBERSHIP MICE WINONA) (AFRAIDOF WOLVES SHEEP) (MEMBERSHIP CATS EMILY)
  (AFRAIDOF SHEEP SHEEP) (MEMBERSHIP CATS WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" (CATS))
  ("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   42 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 41 :STORY
 ("gertrude is a sheep." "jessica is a wolf." "gertrude is a wolf."
  "mice are afraid of cats." "sheep are afraid of cats."
  "wolves are afraid of wolves." "cats are afraid of sheep."
  "emily is a mouse.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP SHEEP GERTRUDE) (MEMBERSHIP WOLVES JESSICA)
  (MEMBERSHIP WOLVES GERTRUDE) (AFRAIDOF MICE CATS) (AFRAIDOF SHEEP CATS)
  (AFRAIDOF WOLVES WOLVES) (AFRAIDOF CATS SHEEP) (MEMBERSHIP MICE EMILY))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (CATS)) ("what is emily afraid of?" (CATS))
  ("what is winona afraid of?" "Incomplete")
  ("what is jessica afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   43 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 42 :STORY
 ("wolves are afraid of wolves." "cats are afraid of cats."
  "sheep are afraid of sheep." "emily is a mouse." "cats are afraid of mice."
  "sheep are afraid of mice." "winona is a mouse."
  "wolves are afraid of mice.")
 :QUESTIONS
 ("what is emily afraid of?" "what is jessica afraid of?"
  "what is gertrude afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES WOLVES) (AFRAIDOF CATS CATS) (AFRAIDOF SHEEP SHEEP)
  (MEMBERSHIP MICE EMILY) (AFRAIDOF CATS MICE) (AFRAIDOF SHEEP MICE)
  (MEMBERSHIP MICE WINONA) (AFRAIDOF WOLVES MICE))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is emily afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   44 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 43 :STORY
 ("sheep are afraid of wolves." "gertrude is a mouse."
  "sheep are afraid of mice." "cats are afraid of mice." "jessica is a wolf."
  "winona is a cat." "cats are afraid of sheep." "jessica is a mouse.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is gertrude afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP WOLVES) (MEMBERSHIP MICE GERTRUDE) (AFRAIDOF SHEEP MICE)
  (AFRAIDOF CATS MICE) (MEMBERSHIP WOLVES JESSICA) (MEMBERSHIP CATS WINONA)
  (AFRAIDOF CATS SHEEP) (MEMBERSHIP MICE JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" (MICE)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   45 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 44 :STORY
 ("cats are afraid of wolves." "sheep are afraid of cats."
  "gertrude is a wolf." "sheep are afraid of wolves."
  "cats are afraid of mice." "wolves are afraid of cats." "jessica is a mouse."
  "wolves are afraid of mice.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS WOLVES) (AFRAIDOF SHEEP CATS) (MEMBERSHIP WOLVES GERTRUDE)
  (AFRAIDOF SHEEP WOLVES) (AFRAIDOF CATS MICE) (AFRAIDOF WOLVES CATS)
  (MEMBERSHIP MICE JESSICA) (AFRAIDOF WOLVES MICE))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (CATS))
  ("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   46 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 45 :STORY
 ("mice are afraid of cats." "winona is a mouse." "sheep are afraid of sheep."
  "winona is a sheep." "winona is a cat." "emily is a mouse."
  "jessica is a mouse." "gertrude is a cat.")
 :QUESTIONS
 ("what is emily afraid of?" "what is winona afraid of?"
  "what is gertrude afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE CATS) (MEMBERSHIP MICE WINONA) (AFRAIDOF SHEEP SHEEP)
  (MEMBERSHIP SHEEP WINONA) (MEMBERSHIP CATS WINONA) (MEMBERSHIP MICE EMILY)
  (MEMBERSHIP MICE JESSICA) (MEMBERSHIP CATS GERTRUDE))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is emily afraid of?" (CATS)) ("what is winona afraid of?" (CATS))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   47 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 46 :STORY
 ("jessica is a wolf." "gertrude is a sheep." "jessica is a cat."
  "winona is a wolf." "emily is a mouse." "winona is a mouse."
  "emily is a wolf." "cats are afraid of cats.")
 :QUESTIONS
 ("what is winona afraid of?" "what is emily afraid of?"
  "what is gertrude afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES JESSICA) (MEMBERSHIP SHEEP GERTRUDE)
  (MEMBERSHIP CATS JESSICA) (MEMBERSHIP WOLVES WINONA) (MEMBERSHIP MICE EMILY)
  (MEMBERSHIP MICE WINONA) (MEMBERSHIP WOLVES EMILY) (AFRAIDOF CATS CATS))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is winona afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   48 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 47 :STORY
 ("winona is a mouse." "mice are afraid of cats." "jessica is a wolf."
  "emily is a cat." "gertrude is a sheep." "winona is a sheep."
  "jessica is a cat." "gertrude is a mouse.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is winona afraid of?"
  "what is jessica afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE WINONA) (AFRAIDOF MICE CATS) (MEMBERSHIP WOLVES JESSICA)
  (MEMBERSHIP CATS EMILY) (MEMBERSHIP SHEEP GERTRUDE) (MEMBERSHIP SHEEP WINONA)
  (MEMBERSHIP CATS JESSICA) (MEMBERSHIP MICE GERTRUDE))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (CATS)) ("what is winona afraid of?" (CATS))
  ("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   49 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 48 :STORY
 ("jessica is a wolf." "sheep are afraid of sheep." "winona is a sheep."
  "gertrude is a wolf." "gertrude is a cat." "sheep are afraid of wolves."
  "sheep are afraid of cats." "winona is a mouse.")
 :QUESTIONS
 ("what is winona afraid of?" "what is gertrude afraid of?"
  "what is jessica afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES JESSICA) (AFRAIDOF SHEEP SHEEP) (MEMBERSHIP SHEEP WINONA)
  (MEMBERSHIP WOLVES GERTRUDE) (MEMBERSHIP CATS GERTRUDE)
  (AFRAIDOF SHEEP WOLVES) (AFRAIDOF SHEEP CATS) (MEMBERSHIP MICE WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is winona afraid of?" (SHEEP))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   50 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 49 :STORY
 ("gertrude is a sheep." "mice are afraid of mice." "emily is a wolf."
  "emily is a sheep." "wolves are afraid of wolves."
  "cats are afraid of wolves." "wolves are afraid of sheep."
  "mice are afraid of cats.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is emily afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP SHEEP GERTRUDE) (AFRAIDOF MICE MICE) (MEMBERSHIP WOLVES EMILY)
  (MEMBERSHIP SHEEP EMILY) (AFRAIDOF WOLVES WOLVES) (AFRAIDOF CATS WOLVES)
  (AFRAIDOF WOLVES SHEEP) (AFRAIDOF MICE CATS))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete")
  ("what is emily afraid of?" (WOLVES))
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   51 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 50 :STORY
 ("mice are afraid of sheep." "cats are afraid of sheep." "jessica is a cat."
  "winona is a sheep." "emily is a mouse." "emily is a sheep."
  "gertrude is a cat." "emily is a cat.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE SHEEP) (AFRAIDOF CATS SHEEP) (MEMBERSHIP CATS JESSICA)
  (MEMBERSHIP SHEEP WINONA) (MEMBERSHIP MICE EMILY) (MEMBERSHIP SHEEP EMILY)
  (MEMBERSHIP CATS GERTRUDE) (MEMBERSHIP CATS EMILY))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (SHEEP))
  ("what is jessica afraid of?" (SHEEP)) ("what is emily afraid of?" (SHEEP))
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   52 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 51 :STORY
 ("sheep are afraid of mice." "cats are afraid of mice."
  "wolves are afraid of cats." "jessica is a cat." "cats are afraid of wolves."
  "cats are afraid of cats." "jessica is a wolf." "wolves are afraid of mice.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP MICE) (AFRAIDOF CATS MICE) (AFRAIDOF WOLVES CATS)
  (MEMBERSHIP CATS JESSICA) (AFRAIDOF CATS WOLVES) (AFRAIDOF CATS CATS)
  (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF WOLVES MICE))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" (MICE))
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   53 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 52 :STORY
 ("emily is a wolf." "jessica is a wolf." "jessica is a mouse."
  "sheep are afraid of mice." "winona is a sheep." "cats are afraid of sheep."
  "jessica is a cat." "jessica is a sheep.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is winona afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES EMILY) (MEMBERSHIP WOLVES JESSICA)
  (MEMBERSHIP MICE JESSICA) (AFRAIDOF SHEEP MICE) (MEMBERSHIP SHEEP WINONA)
  (AFRAIDOF CATS SHEEP) (MEMBERSHIP CATS JESSICA) (MEMBERSHIP SHEEP JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" (SHEEP)) ("what is winona afraid of?" (MICE))
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   54 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 53 :STORY
 ("sheep are afraid of sheep." "sheep are afraid of mice."
  "cats are afraid of mice." "cats are afraid of wolves." "emily is a mouse."
  "gertrude is a mouse." "sheep are afraid of wolves."
  "cats are afraid of cats.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP SHEEP) (AFRAIDOF SHEEP MICE) (AFRAIDOF CATS MICE)
  (AFRAIDOF CATS WOLVES) (MEMBERSHIP MICE EMILY) (MEMBERSHIP MICE GERTRUDE)
  (AFRAIDOF SHEEP WOLVES) (AFRAIDOF CATS CATS))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   55 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 54 :STORY
 ("wolves are afraid of mice." "winona is a cat." "wolves are afraid of sheep."
  "sheep are afraid of mice." "sheep are afraid of wolves."
  "winona is a sheep." "wolves are afraid of cats."
  "cats are afraid of wolves.")
 :QUESTIONS
 ("what is winona afraid of?" "what is emily afraid of?"
  "what is jessica afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES MICE) (MEMBERSHIP CATS WINONA) (AFRAIDOF WOLVES SHEEP)
  (AFRAIDOF SHEEP MICE) (AFRAIDOF SHEEP WOLVES) (MEMBERSHIP SHEEP WINONA)
  (AFRAIDOF WOLVES CATS) (AFRAIDOF CATS WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is winona afraid of?" (WOLVES))
  ("what is emily afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   56 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 55 :STORY
 ("wolves are afraid of cats." "emily is a sheep." "winona is a mouse."
  "mice are afraid of cats." "gertrude is a wolf." "gertrude is a sheep."
  "emily is a mouse." "mice are afraid of mice.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES CATS) (MEMBERSHIP SHEEP EMILY) (MEMBERSHIP MICE WINONA)
  (AFRAIDOF MICE CATS) (MEMBERSHIP WOLVES GERTRUDE) (MEMBERSHIP SHEEP GERTRUDE)
  (MEMBERSHIP MICE EMILY) (AFRAIDOF MICE MICE))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (CATS))
  ("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" (CATS)) ("what is winona afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   57 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 56 :STORY
 ("wolves are afraid of cats." "mice are afraid of wolves."
  "jessica is a wolf." "gertrude is a wolf." "sheep are afraid of wolves."
  "emily is a wolf." "winona is a cat." "cats are afraid of wolves.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is emily afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES CATS) (AFRAIDOF MICE WOLVES) (MEMBERSHIP WOLVES JESSICA)
  (MEMBERSHIP WOLVES GERTRUDE) (AFRAIDOF SHEEP WOLVES)
  (MEMBERSHIP WOLVES EMILY) (MEMBERSHIP CATS WINONA) (AFRAIDOF CATS WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" (CATS)) ("what is winona afraid of?" (WOLVES))
  ("what is emily afraid of?" (CATS)) ("what is gertrude afraid of?" (CATS)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   58 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 57 :STORY
 ("jessica is a mouse." "gertrude is a mouse." "cats are afraid of sheep."
  "jessica is a wolf." "jessica is a sheep." "winona is a mouse."
  "gertrude is a wolf." "mice are afraid of mice.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is gertrude afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE JESSICA) (MEMBERSHIP MICE GERTRUDE) (AFRAIDOF CATS SHEEP)
  (MEMBERSHIP WOLVES JESSICA) (MEMBERSHIP SHEEP JESSICA)
  (MEMBERSHIP MICE WINONA) (MEMBERSHIP WOLVES GERTRUDE) (AFRAIDOF MICE MICE))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is jessica afraid of?" (MICE)) ("what is winona afraid of?" (MICE))
  ("what is gertrude afraid of?" (MICE))
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   59 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 58 :STORY
 ("emily is a mouse." "sheep are afraid of cats." "winona is a wolf."
  "emily is a wolf." "winona is a mouse." "winona is a cat."
  "winona is a sheep." "jessica is a wolf.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is winona afraid of?"
  "what is jessica afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE EMILY) (AFRAIDOF SHEEP CATS) (MEMBERSHIP WOLVES WINONA)
  (MEMBERSHIP WOLVES EMILY) (MEMBERSHIP MICE WINONA) (MEMBERSHIP CATS WINONA)
  (MEMBERSHIP SHEEP WINONA) (MEMBERSHIP WOLVES JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is gertrude afraid of?" "Incomplete")
  ("what is winona afraid of?" (CATS))
  ("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   60 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 59 :STORY
 ("wolves are afraid of wolves." "cats are afraid of sheep."
  "gertrude is a mouse." "winona is a wolf." "jessica is a wolf."
  "emily is a cat." "sheep are afraid of cats." "cats are afraid of wolves.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is gertrude afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES WOLVES) (AFRAIDOF CATS SHEEP) (MEMBERSHIP MICE GERTRUDE)
  (MEMBERSHIP WOLVES WINONA) (MEMBERSHIP WOLVES JESSICA)
  (MEMBERSHIP CATS EMILY) (AFRAIDOF SHEEP CATS) (AFRAIDOF CATS WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is jessica afraid of?" (WOLVES))
  ("what is winona afraid of?" (WOLVES))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" (SHEEP)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   61 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 60 :STORY
 ("mice are afraid of wolves." "emily is a cat." "wolves are afraid of sheep."
  "jessica is a cat." "cats are afraid of sheep." "gertrude is a cat."
  "cats are afraid of mice." "mice are afraid of cats.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is gertrude afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE WOLVES) (MEMBERSHIP CATS EMILY) (AFRAIDOF WOLVES SHEEP)
  (MEMBERSHIP CATS JESSICA) (AFRAIDOF CATS SHEEP) (MEMBERSHIP CATS GERTRUDE)
  (AFRAIDOF CATS MICE) (AFRAIDOF MICE CATS))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is jessica afraid of?" (SHEEP))
  ("what is gertrude afraid of?" (SHEEP)) ("what is emily afraid of?" (SHEEP))
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   62 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 61 :STORY
 ("gertrude is a wolf." "gertrude is a cat." "wolves are afraid of wolves."
  "emily is a mouse." "gertrude is a mouse." "jessica is a mouse."
  "winona is a mouse." "winona is a sheep.")
 :QUESTIONS
 ("what is emily afraid of?" "what is gertrude afraid of?"
  "what is winona afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES GERTRUDE) (MEMBERSHIP CATS GERTRUDE)
  (AFRAIDOF WOLVES WOLVES) (MEMBERSHIP MICE EMILY) (MEMBERSHIP MICE GERTRUDE)
  (MEMBERSHIP MICE JESSICA) (MEMBERSHIP MICE WINONA) (MEMBERSHIP SHEEP WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (WOLVES))
  ("what is winona afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   63 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 62 :STORY
 ("cats are afraid of mice." "jessica is a sheep." "gertrude is a sheep."
  "winona is a wolf." "winona is a mouse." "wolves are afraid of wolves."
  "mice are afraid of mice." "cats are afraid of sheep.")
 :QUESTIONS
 ("what is emily afraid of?" "what is jessica afraid of?"
  "what is gertrude afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS MICE) (MEMBERSHIP SHEEP JESSICA) (MEMBERSHIP SHEEP GERTRUDE)
  (MEMBERSHIP WOLVES WINONA) (MEMBERSHIP MICE WINONA) (AFRAIDOF WOLVES WOLVES)
  (AFRAIDOF MICE MICE) (AFRAIDOF CATS SHEEP))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is emily afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete")
  ("what is winona afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   64 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 63 :STORY
 ("cats are afraid of cats." "cats are afraid of mice." "jessica is a sheep."
  "emily is a sheep." "gertrude is a sheep." "sheep are afraid of cats."
  "winona is a cat." "emily is a cat.")
 :QUESTIONS
 ("what is winona afraid of?" "what is emily afraid of?"
  "what is gertrude afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS CATS) (AFRAIDOF CATS MICE) (MEMBERSHIP SHEEP JESSICA)
  (MEMBERSHIP SHEEP EMILY) (MEMBERSHIP SHEEP GERTRUDE) (AFRAIDOF SHEEP CATS)
  (MEMBERSHIP CATS WINONA) (MEMBERSHIP CATS EMILY))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is winona afraid of?" (CATS)) ("what is emily afraid of?" (CATS))
  ("what is gertrude afraid of?" (CATS)) ("what is jessica afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   65 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 64 :STORY
 ("jessica is a sheep." "winona is a mouse." "gertrude is a wolf."
  "wolves are afraid of cats." "wolves are afraid of sheep."
  "gertrude is a mouse." "emily is a cat." "winona is a cat.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is gertrude afraid of?"
  "what is winona afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP SHEEP JESSICA) (MEMBERSHIP MICE WINONA)
  (MEMBERSHIP WOLVES GERTRUDE) (AFRAIDOF WOLVES CATS) (AFRAIDOF WOLVES SHEEP)
  (MEMBERSHIP MICE GERTRUDE) (MEMBERSHIP CATS EMILY) (MEMBERSHIP CATS WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (CATS))
  ("what is winona afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   66 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 65 :STORY
 ("emily is a wolf." "wolves are afraid of wolves." "emily is a mouse."
  "emily is a sheep." "sheep are afraid of cats." "gertrude is a mouse."
  "wolves are afraid of mice." "sheep are afraid of mice.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES EMILY) (AFRAIDOF WOLVES WOLVES) (MEMBERSHIP MICE EMILY)
  (MEMBERSHIP SHEEP EMILY) (AFRAIDOF SHEEP CATS) (MEMBERSHIP MICE GERTRUDE)
  (AFRAIDOF WOLVES MICE) (AFRAIDOF SHEEP MICE))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" (WOLVES))
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   67 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 66 :STORY
 ("cats are afraid of cats." "wolves are afraid of sheep."
  "mice are afraid of cats." "gertrude is a cat." "mice are afraid of sheep."
  "wolves are afraid of mice." "gertrude is a sheep." "jessica is a cat.")
 :QUESTIONS
 ("what is winona afraid of?" "what is jessica afraid of?"
  "what is gertrude afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS CATS) (AFRAIDOF WOLVES SHEEP) (AFRAIDOF MICE CATS)
  (MEMBERSHIP CATS GERTRUDE) (AFRAIDOF MICE SHEEP) (AFRAIDOF WOLVES MICE)
  (MEMBERSHIP SHEEP GERTRUDE) (MEMBERSHIP CATS JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is winona afraid of?" "Incomplete")
  ("what is jessica afraid of?" (CATS)) ("what is gertrude afraid of?" (CATS))
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   68 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 67 :STORY
 ("winona is a mouse." "jessica is a cat." "jessica is a wolf."
  "cats are afraid of sheep." "wolves are afraid of wolves."
  "cats are afraid of cats." "winona is a wolf." "emily is a mouse.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is winona afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE WINONA) (MEMBERSHIP CATS JESSICA)
  (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF CATS SHEEP) (AFRAIDOF WOLVES WOLVES)
  (AFRAIDOF CATS CATS) (MEMBERSHIP WOLVES WINONA) (MEMBERSHIP MICE EMILY))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" (SHEEP)) ("what is winona afraid of?" (WOLVES))
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   69 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 68 :STORY
 ("sheep are afraid of sheep." "winona is a mouse." "emily is a wolf."
  "mice are afraid of wolves." "cats are afraid of sheep." "emily is a cat."
  "jessica is a mouse." "wolves are afraid of wolves.")
 :QUESTIONS
 ("what is winona afraid of?" "what is emily afraid of?"
  "what is gertrude afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP SHEEP) (MEMBERSHIP MICE WINONA) (MEMBERSHIP WOLVES EMILY)
  (AFRAIDOF MICE WOLVES) (AFRAIDOF CATS SHEEP) (MEMBERSHIP CATS EMILY)
  (MEMBERSHIP MICE JESSICA) (AFRAIDOF WOLVES WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is winona afraid of?" (WOLVES)) ("what is emily afraid of?" (WOLVES))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   70 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 69 :STORY
 ("sheep are afraid of wolves." "jessica is a sheep."
  "cats are afraid of sheep." "emily is a sheep." "jessica is a mouse."
  "gertrude is a sheep." "mice are afraid of wolves."
  "sheep are afraid of sheep.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP WOLVES) (MEMBERSHIP SHEEP JESSICA) (AFRAIDOF CATS SHEEP)
  (MEMBERSHIP SHEEP EMILY) (MEMBERSHIP MICE JESSICA)
  (MEMBERSHIP SHEEP GERTRUDE) (AFRAIDOF MICE WOLVES) (AFRAIDOF SHEEP SHEEP))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" (WOLVES)) ("what is emily afraid of?" (WOLVES))
  ("what is winona afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   71 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 70 :STORY
 ("emily is a mouse." "gertrude is a wolf." "wolves are afraid of wolves."
  "emily is a cat." "cats are afraid of mice." "mice are afraid of cats."
  "gertrude is a mouse." "sheep are afraid of cats.")
 :QUESTIONS
 ("what is winona afraid of?" "what is emily afraid of?"
  "what is jessica afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE EMILY) (MEMBERSHIP WOLVES GERTRUDE) (AFRAIDOF WOLVES WOLVES)
  (MEMBERSHIP CATS EMILY) (AFRAIDOF CATS MICE) (AFRAIDOF MICE CATS)
  (MEMBERSHIP MICE GERTRUDE) (AFRAIDOF SHEEP CATS))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is winona afraid of?" "Incomplete")
  ("what is emily afraid of?" (CATS))
  ("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   72 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 71 :STORY
 ("emily is a cat." "mice are afraid of cats." "emily is a mouse."
  "jessica is a sheep." "cats are afraid of wolves." "emily is a wolf."
  "cats are afraid of mice." "winona is a sheep.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is gertrude afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS EMILY) (AFRAIDOF MICE CATS) (MEMBERSHIP MICE EMILY)
  (MEMBERSHIP SHEEP JESSICA) (AFRAIDOF CATS WOLVES) (MEMBERSHIP WOLVES EMILY)
  (AFRAIDOF CATS MICE) (MEMBERSHIP SHEEP WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" (WOLVES))
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   73 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 72 :STORY
 ("mice are afraid of mice." "winona is a cat." "gertrude is a cat."
  "emily is a wolf." "jessica is a wolf." "cats are afraid of mice."
  "gertrude is a sheep." "emily is a cat.")
 :QUESTIONS
 ("what is winona afraid of?" "what is emily afraid of?"
  "what is jessica afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE MICE) (MEMBERSHIP CATS WINONA) (MEMBERSHIP CATS GERTRUDE)
  (MEMBERSHIP WOLVES EMILY) (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF CATS MICE)
  (MEMBERSHIP SHEEP GERTRUDE) (MEMBERSHIP CATS EMILY))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is winona afraid of?" (MICE)) ("what is emily afraid of?" (MICE))
  ("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (MICE)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   74 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 73 :STORY
 ("wolves are afraid of cats." "cats are afraid of mice." "emily is a mouse."
  "mice are afraid of cats." "gertrude is a mouse." "emily is a sheep."
  "cats are afraid of cats." "wolves are afraid of wolves.")
 :QUESTIONS
 ("what is emily afraid of?" "what is jessica afraid of?"
  "what is gertrude afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES CATS) (AFRAIDOF CATS MICE) (MEMBERSHIP MICE EMILY)
  (AFRAIDOF MICE CATS) (MEMBERSHIP MICE GERTRUDE) (MEMBERSHIP SHEEP EMILY)
  (AFRAIDOF CATS CATS) (AFRAIDOF WOLVES WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is emily afraid of?" (CATS))
  ("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (CATS))
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   75 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 74 :STORY
 ("gertrude is a cat." "cats are afraid of wolves." "winona is a cat."
  "cats are afraid of sheep." "wolves are afraid of wolves."
  "wolves are afraid of cats." "winona is a wolf." "gertrude is a sheep.")
 :QUESTIONS
 ("what is winona afraid of?" "what is jessica afraid of?"
  "what is gertrude afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS GERTRUDE) (AFRAIDOF CATS WOLVES) (MEMBERSHIP CATS WINONA)
  (AFRAIDOF CATS SHEEP) (AFRAIDOF WOLVES WOLVES) (AFRAIDOF WOLVES CATS)
  (MEMBERSHIP WOLVES WINONA) (MEMBERSHIP SHEEP GERTRUDE))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is winona afraid of?" (WOLVES))
  ("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (WOLVES))
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   76 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 75 :STORY
 ("winona is a wolf." "gertrude is a mouse." "emily is a sheep."
  "jessica is a cat." "gertrude is a wolf." "wolves are afraid of cats."
  "gertrude is a cat." "mice are afraid of mice.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is winona afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES WINONA) (MEMBERSHIP MICE GERTRUDE)
  (MEMBERSHIP SHEEP EMILY) (MEMBERSHIP CATS JESSICA)
  (MEMBERSHIP WOLVES GERTRUDE) (AFRAIDOF WOLVES CATS)
  (MEMBERSHIP CATS GERTRUDE) (AFRAIDOF MICE MICE))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (MICE))
  ("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" (CATS))
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   77 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 76 :STORY
 ("jessica is a mouse." "sheep are afraid of wolves."
  "wolves are afraid of mice." "jessica is a sheep."
  "wolves are afraid of cats." "sheep are afraid of sheep."
  "wolves are afraid of wolves." "winona is a wolf.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is gertrude afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE JESSICA) (AFRAIDOF SHEEP WOLVES) (AFRAIDOF WOLVES MICE)
  (MEMBERSHIP SHEEP JESSICA) (AFRAIDOF WOLVES CATS) (AFRAIDOF SHEEP SHEEP)
  (AFRAIDOF WOLVES WOLVES) (MEMBERSHIP WOLVES WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is jessica afraid of?" (WOLVES)) ("what is winona afraid of?" (MICE))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   78 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 77 :STORY
 ("mice are afraid of cats." "cats are afraid of wolves." "jessica is a cat."
  "wolves are afraid of cats." "cats are afraid of sheep." "gertrude is a cat."
  "wolves are afraid of sheep." "wolves are afraid of mice.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is emily afraid of?"
  "what is jessica afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE CATS) (AFRAIDOF CATS WOLVES) (MEMBERSHIP CATS JESSICA)
  (AFRAIDOF WOLVES CATS) (AFRAIDOF CATS SHEEP) (MEMBERSHIP CATS GERTRUDE)
  (AFRAIDOF WOLVES SHEEP) (AFRAIDOF WOLVES MICE))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (WOLVES))
  ("what is emily afraid of?" "Incomplete")
  ("what is jessica afraid of?" (WOLVES))
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   79 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 78 :STORY
 ("sheep are afraid of cats." "wolves are afraid of sheep."
  "winona is a sheep." "gertrude is a cat." "sheep are afraid of mice."
  "jessica is a wolf." "cats are afraid of mice." "winona is a mouse.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP CATS) (AFRAIDOF WOLVES SHEEP) (MEMBERSHIP SHEEP WINONA)
  (MEMBERSHIP CATS GERTRUDE) (AFRAIDOF SHEEP MICE) (MEMBERSHIP WOLVES JESSICA)
  (AFRAIDOF CATS MICE) (MEMBERSHIP MICE WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (MICE)) ("what is jessica afraid of?" (SHEEP))
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   80 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 79 :STORY
 ("wolves are afraid of sheep." "cats are afraid of mice."
  "mice are afraid of wolves." "winona is a mouse." "emily is a sheep."
  "winona is a cat." "sheep are afraid of wolves." "jessica is a wolf.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES SHEEP) (AFRAIDOF CATS MICE) (AFRAIDOF MICE WOLVES)
  (MEMBERSHIP MICE WINONA) (MEMBERSHIP SHEEP EMILY) (MEMBERSHIP CATS WINONA)
  (AFRAIDOF SHEEP WOLVES) (MEMBERSHIP WOLVES JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" (SHEEP)) ("what is emily afraid of?" (WOLVES))
  ("what is winona afraid of?" (WOLVES))
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   81 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 80 :STORY
 ("sheep are afraid of cats." "cats are afraid of cats."
  "sheep are afraid of sheep." "mice are afraid of sheep."
  "cats are afraid of sheep." "emily is a wolf." "cats are afraid of mice."
  "gertrude is a mouse.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is gertrude afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP CATS) (AFRAIDOF CATS CATS) (AFRAIDOF SHEEP SHEEP)
  (AFRAIDOF MICE SHEEP) (AFRAIDOF CATS SHEEP) (MEMBERSHIP WOLVES EMILY)
  (AFRAIDOF CATS MICE) (MEMBERSHIP MICE GERTRUDE))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (SHEEP))
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   82 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 81 :STORY
 ("sheep are afraid of sheep." "gertrude is a cat." "winona is a cat."
  "mice are afraid of wolves." "cats are afraid of mice."
  "gertrude is a mouse." "emily is a sheep." "jessica is a mouse.")
 :QUESTIONS
 ("what is emily afraid of?" "what is winona afraid of?"
  "what is jessica afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP SHEEP) (MEMBERSHIP CATS GERTRUDE) (MEMBERSHIP CATS WINONA)
  (AFRAIDOF MICE WOLVES) (AFRAIDOF CATS MICE) (MEMBERSHIP MICE GERTRUDE)
  (MEMBERSHIP SHEEP EMILY) (MEMBERSHIP MICE JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is emily afraid of?" (SHEEP)) ("what is winona afraid of?" (MICE))
  ("what is jessica afraid of?" (WOLVES))
  ("what is gertrude afraid of?" (MICE)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   83 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 82 :STORY
 ("mice are afraid of wolves." "winona is a cat." "emily is a cat."
  "emily is a sheep." "mice are afraid of sheep." "emily is a wolf."
  "cats are afraid of sheep." "gertrude is a wolf.")
 :QUESTIONS
 ("what is emily afraid of?" "what is gertrude afraid of?"
  "what is winona afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE WOLVES) (MEMBERSHIP CATS WINONA) (MEMBERSHIP CATS EMILY)
  (MEMBERSHIP SHEEP EMILY) (AFRAIDOF MICE SHEEP) (MEMBERSHIP WOLVES EMILY)
  (AFRAIDOF CATS SHEEP) (MEMBERSHIP WOLVES GERTRUDE))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is emily afraid of?" (SHEEP))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is winona afraid of?" (SHEEP))
  ("what is jessica afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   84 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 83 :STORY
 ("wolves are afraid of sheep." "wolves are afraid of cats."
  "winona is a wolf." "gertrude is a cat." "jessica is a cat."
  "gertrude is a sheep." "mice are afraid of mice."
  "mice are afraid of wolves.")
 :QUESTIONS
 ("what is emily afraid of?" "what is gertrude afraid of?"
  "what is jessica afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF WOLVES SHEEP) (AFRAIDOF WOLVES CATS) (MEMBERSHIP WOLVES WINONA)
  (MEMBERSHIP CATS GERTRUDE) (MEMBERSHIP CATS JESSICA)
  (MEMBERSHIP SHEEP GERTRUDE) (AFRAIDOF MICE MICE) (AFRAIDOF MICE WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is emily afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" (SHEEP)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   85 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 84 :STORY
 ("gertrude is a sheep." "jessica is a mouse." "mice are afraid of sheep."
  "sheep are afraid of sheep." "gertrude is a cat." "gertrude is a wolf."
  "sheep are afraid of cats." "mice are afraid of wolves.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is winona afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP SHEEP GERTRUDE) (MEMBERSHIP MICE JESSICA) (AFRAIDOF MICE SHEEP)
  (AFRAIDOF SHEEP SHEEP) (MEMBERSHIP CATS GERTRUDE)
  (MEMBERSHIP WOLVES GERTRUDE) (AFRAIDOF SHEEP CATS) (AFRAIDOF MICE WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (SHEEP))
  ("what is jessica afraid of?" (SHEEP))
  ("what is winona afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   86 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 85 :STORY
 ("cats are afraid of cats." "emily is a cat." "cats are afraid of mice."
  "jessica is a mouse." "cats are afraid of sheep." "winona is a wolf."
  "winona is a cat." "wolves are afraid of cats.")
 :QUESTIONS
 ("what is emily afraid of?" "what is gertrude afraid of?"
  "what is jessica afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS CATS) (MEMBERSHIP CATS EMILY) (AFRAIDOF CATS MICE)
  (MEMBERSHIP MICE JESSICA) (AFRAIDOF CATS SHEEP) (MEMBERSHIP WOLVES WINONA)
  (MEMBERSHIP CATS WINONA) (AFRAIDOF WOLVES CATS))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is emily afraid of?" (CATS))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   87 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 86 :STORY
 ("jessica is a cat." "jessica is a sheep." "sheep are afraid of sheep."
  "wolves are afraid of wolves." "winona is a cat." "jessica is a wolf."
  "gertrude is a sheep." "jessica is a mouse.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is gertrude afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS JESSICA) (MEMBERSHIP SHEEP JESSICA) (AFRAIDOF SHEEP SHEEP)
  (AFRAIDOF WOLVES WOLVES) (MEMBERSHIP CATS WINONA) (MEMBERSHIP WOLVES JESSICA)
  (MEMBERSHIP SHEEP GERTRUDE) (MEMBERSHIP MICE JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is jessica afraid of?" (SHEEP))
  ("what is gertrude afraid of?" (SHEEP))
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   88 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 87 :STORY
 ("jessica is a cat." "jessica is a mouse." "sheep are afraid of sheep."
  "jessica is a sheep." "emily is a sheep." "wolves are afraid of wolves."
  "mice are afraid of sheep." "gertrude is a wolf.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS JESSICA) (MEMBERSHIP MICE JESSICA) (AFRAIDOF SHEEP SHEEP)
  (MEMBERSHIP SHEEP JESSICA) (MEMBERSHIP SHEEP EMILY) (AFRAIDOF WOLVES WOLVES)
  (AFRAIDOF MICE SHEEP) (MEMBERSHIP WOLVES GERTRUDE))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is jessica afraid of?" (SHEEP)) ("what is emily afraid of?" (SHEEP))
  ("what is winona afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   89 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 88 :STORY
 ("gertrude is a sheep." "sheep are afraid of wolves."
  "sheep are afraid of cats." "jessica is a sheep." "winona is a sheep."
  "sheep are afraid of sheep." "emily is a wolf."
  "wolves are afraid of wolves.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is gertrude afraid of?"
  "what is winona afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP SHEEP GERTRUDE) (AFRAIDOF SHEEP WOLVES) (AFRAIDOF SHEEP CATS)
  (MEMBERSHIP SHEEP JESSICA) (MEMBERSHIP SHEEP WINONA) (AFRAIDOF SHEEP SHEEP)
  (MEMBERSHIP WOLVES EMILY) (AFRAIDOF WOLVES WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is jessica afraid of?" (WOLVES))
  ("what is gertrude afraid of?" (WOLVES))
  ("what is winona afraid of?" (WOLVES)) ("what is emily afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   90 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 89 :STORY
 ("gertrude is a mouse." "emily is a mouse." "winona is a cat."
  "cats are afraid of wolves." "emily is a sheep."
  "wolves are afraid of wolves." "jessica is a mouse."
  "sheep are afraid of wolves.")
 :QUESTIONS
 ("what is emily afraid of?" "what is winona afraid of?"
  "what is jessica afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE GERTRUDE) (MEMBERSHIP MICE EMILY) (MEMBERSHIP CATS WINONA)
  (AFRAIDOF CATS WOLVES) (MEMBERSHIP SHEEP EMILY) (AFRAIDOF WOLVES WOLVES)
  (MEMBERSHIP MICE JESSICA) (AFRAIDOF SHEEP WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is emily afraid of?" (WOLVES)) ("what is winona afraid of?" (WOLVES))
  ("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   91 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 90 :STORY
 ("cats are afraid of sheep." "emily is a sheep." "wolves are afraid of mice."
  "jessica is a mouse." "wolves are afraid of sheep."
  "mice are afraid of sheep." "cats are afraid of wolves." "winona is a wolf.")
 :QUESTIONS
 ("what is emily afraid of?" "what is jessica afraid of?"
  "what is gertrude afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS SHEEP) (MEMBERSHIP SHEEP EMILY) (AFRAIDOF WOLVES MICE)
  (MEMBERSHIP MICE JESSICA) (AFRAIDOF WOLVES SHEEP) (AFRAIDOF MICE SHEEP)
  (AFRAIDOF CATS WOLVES) (MEMBERSHIP WOLVES WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is emily afraid of?" "Incomplete")
  ("what is jessica afraid of?" (SHEEP))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is winona afraid of?" (MICE)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   92 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 91 :STORY
 ("gertrude is a mouse." "gertrude is a wolf." "mice are afraid of mice."
  "sheep are afraid of sheep." "emily is a cat." "mice are afraid of sheep."
  "emily is a mouse." "cats are afraid of mice.")
 :QUESTIONS
 ("what is winona afraid of?" "what is jessica afraid of?"
  "what is gertrude afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP MICE GERTRUDE) (MEMBERSHIP WOLVES GERTRUDE) (AFRAIDOF MICE MICE)
  (AFRAIDOF SHEEP SHEEP) (MEMBERSHIP CATS EMILY) (AFRAIDOF MICE SHEEP)
  (MEMBERSHIP MICE EMILY) (AFRAIDOF CATS MICE))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is winona afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (MICE)) ("what is emily afraid of?" (MICE)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   93 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 92 :STORY
 ("gertrude is a wolf." "mice are afraid of cats." "jessica is a wolf."
  "wolves are afraid of sheep." "emily is a mouse." "sheep are afraid of cats."
  "jessica is a sheep." "sheep are afraid of sheep.")
 :QUESTIONS
 ("what is jessica afraid of?" "what is winona afraid of?"
  "what is gertrude afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES GERTRUDE) (AFRAIDOF MICE CATS) (MEMBERSHIP WOLVES JESSICA)
  (AFRAIDOF WOLVES SHEEP) (MEMBERSHIP MICE EMILY) (AFRAIDOF SHEEP CATS)
  (MEMBERSHIP SHEEP JESSICA) (AFRAIDOF SHEEP SHEEP))
 :QUESTION_FORMS
 ((AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is jessica afraid of?" (SHEEP))
  ("what is winona afraid of?" "Incomplete")
  ("what is gertrude afraid of?" (SHEEP)) ("what is emily afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   94 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 93 :STORY
 ("winona is a wolf." "wolves are afraid of mice." "mice are afraid of mice."
  "emily is a mouse." "gertrude is a wolf." "jessica is a cat."
  "winona is a cat." "cats are afraid of cats.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is winona afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES WINONA) (AFRAIDOF WOLVES MICE) (AFRAIDOF MICE MICE)
  (MEMBERSHIP MICE EMILY) (MEMBERSHIP WOLVES GERTRUDE)
  (MEMBERSHIP CATS JESSICA) (MEMBERSHIP CATS WINONA) (AFRAIDOF CATS CATS))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (MICE)) ("what is jessica afraid of?" (CATS))
  ("what is winona afraid of?" (MICE)) ("what is emily afraid of?" (MICE)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   95 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 94 :STORY
 ("cats are afraid of sheep." "sheep are afraid of sheep."
  "mice are afraid of cats." "emily is a sheep." "mice are afraid of mice."
  "winona is a wolf." "jessica is a wolf." "sheep are afraid of cats.")
 :QUESTIONS
 ("what is emily afraid of?" "what is jessica afraid of?"
  "what is winona afraid of?" "what is gertrude afraid of?")
 :STORY_FORMS
 ((AFRAIDOF CATS SHEEP) (AFRAIDOF SHEEP SHEEP) (AFRAIDOF MICE CATS)
  (MEMBERSHIP SHEEP EMILY) (AFRAIDOF MICE MICE) (MEMBERSHIP WOLVES WINONA)
  (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF SHEEP CATS))
 :QUESTION_FORMS
 ((AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF GERTRUDE ?X))
 :ANSWERS
 (("what is emily afraid of?" (SHEEP))
  ("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" "Incomplete")
  ("what is gertrude afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   96 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 95 :STORY
 ("sheep are afraid of cats." "winona is a sheep." "gertrude is a mouse."
  "emily is a mouse." "winona is a wolf." "jessica is a wolf."
  "mice are afraid of sheep." "jessica is a cat.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is jessica afraid of?"
  "what is emily afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP CATS) (MEMBERSHIP SHEEP WINONA) (MEMBERSHIP MICE GERTRUDE)
  (MEMBERSHIP MICE EMILY) (MEMBERSHIP WOLVES WINONA)
  (MEMBERSHIP WOLVES JESSICA) (AFRAIDOF MICE SHEEP) (MEMBERSHIP CATS JESSICA))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" (SHEEP))
  ("what is jessica afraid of?" "Incomplete")
  ("what is emily afraid of?" (SHEEP)) ("what is winona afraid of?" (CATS)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   97 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 96 :STORY
 ("winona is a wolf." "cats are afraid of wolves."
  "wolves are afraid of wolves." "wolves are afraid of mice."
  "jessica is a mouse." "mice are afraid of wolves." "winona is a mouse."
  "winona is a sheep.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is emily afraid of?"
  "what is winona afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP WOLVES WINONA) (AFRAIDOF CATS WOLVES) (AFRAIDOF WOLVES WOLVES)
  (AFRAIDOF WOLVES MICE) (MEMBERSHIP MICE JESSICA) (AFRAIDOF MICE WOLVES)
  (MEMBERSHIP MICE WINONA) (MEMBERSHIP SHEEP WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF WINONA ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" "Incomplete")
  ("what is winona afraid of?" (WOLVES))
  ("what is jessica afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   98 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 97 :STORY
 ("sheep are afraid of cats." "mice are afraid of mice."
  "wolves are afraid of mice." "wolves are afraid of cats."
  "wolves are afraid of wolves." "emily is a mouse." "cats are afraid of cats."
  "winona is a mouse.")
 :QUESTIONS
 ("what is gertrude afraid of?" "what is emily afraid of?"
  "what is jessica afraid of?" "what is winona afraid of?")
 :STORY_FORMS
 ((AFRAIDOF SHEEP CATS) (AFRAIDOF MICE MICE) (AFRAIDOF WOLVES MICE)
  (AFRAIDOF WOLVES CATS) (AFRAIDOF WOLVES WOLVES) (MEMBERSHIP MICE EMILY)
  (AFRAIDOF CATS CATS) (MEMBERSHIP MICE WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF WINONA ?X))
 :ANSWERS
 (("what is gertrude afraid of?" "Incomplete")
  ("what is emily afraid of?" (MICE))
  ("what is jessica afraid of?" "Incomplete")
  ("what is winona afraid of?" (MICE)))
 :STORY-IS-CONSISTENT T)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   99 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 98 :STORY
 ("gertrude is a cat." "sheep are afraid of sheep." "winona is a wolf."
  "gertrude is a mouse." "winona is a cat." "mice are afraid of wolves."
  "mice are afraid of mice." "winona is a sheep.")
 :QUESTIONS
 ("what is winona afraid of?" "what is gertrude afraid of?"
  "what is emily afraid of?" "what is jessica afraid of?")
 :STORY_FORMS
 ((MEMBERSHIP CATS GERTRUDE) (AFRAIDOF SHEEP SHEEP) (MEMBERSHIP WOLVES WINONA)
  (MEMBERSHIP MICE GERTRUDE) (MEMBERSHIP CATS WINONA) (AFRAIDOF MICE WOLVES)
  (AFRAIDOF MICE MICE) (MEMBERSHIP SHEEP WINONA))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF EMILY ?X)
  (AFRAIDOF JESSICA ?X))
 :ANSWERS
 (("what is winona afraid of?" (SHEEP))
  ("what is gertrude afraid of?" (WOLVES))
  ("what is emily afraid of?" "Incomplete")
  ("what is jessica afraid of?" "Incomplete"))
 :STORY-IS-CONSISTENT NIL)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;; Story   100 ;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(:NUMBER 99 :STORY
 ("mice are afraid of mice." "sheep are afraid of wolves." "jessica is a wolf."
  "wolves are afraid of sheep." "winona is a wolf." "jessica is a cat."
  "emily is a sheep." "wolves are afraid of wolves.")
 :QUESTIONS
 ("what is winona afraid of?" "what is gertrude afraid of?"
  "what is jessica afraid of?" "what is emily afraid of?")
 :STORY_FORMS
 ((AFRAIDOF MICE MICE) (AFRAIDOF SHEEP WOLVES) (MEMBERSHIP WOLVES JESSICA)
  (AFRAIDOF WOLVES SHEEP) (MEMBERSHIP WOLVES WINONA) (MEMBERSHIP CATS JESSICA)
  (MEMBERSHIP SHEEP EMILY) (AFRAIDOF WOLVES WOLVES))
 :QUESTION_FORMS
 ((AFRAIDOF WINONA ?X) (AFRAIDOF GERTRUDE ?X) (AFRAIDOF JESSICA ?X)
  (AFRAIDOF EMILY ?X))
 :ANSWERS
 (("what is winona afraid of?" (SHEEP))
  ("what is gertrude afraid of?" "Incomplete")
  ("what is jessica afraid of?" (SHEEP)) ("what is emily afraid of?" (WOLVES)))
 :STORY-IS-CONSISTENT NIL)